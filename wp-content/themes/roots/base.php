<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

   <?php /* Google Tag Manager */ ?>
   <?php if( get_field('google_tag_manager_body', 'option') ): ?>
      <?php the_field('google_tag_manager_body', 'option'); ?>
   <?php endif; ?>  

    <!--[if lt IE 8]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
      </div>
    <![endif]-->

  <?php
    do_action('get_header');
    // Choose the correct header
    // pick which one in config.php
    if (current_theme_supports('header-two-navs')) {
      get_template_part( 'templates/header-two-navs' );
    } elseif (current_theme_supports('header-nav-bottom')) {
      get_template_part( 'templates/header-nav-bottom' );
    } else {
      get_template_part( 'templates/header' );
    }
  ?> 

    <?php if (!is_front_page() ): ?>
        <?php if ( get_field('custom_header') ) { ?>
            <div class="custom-header">
                <div class="container">
                    <?php the_field('custom_header'); ?>
                </div>
            </div>
        <?php } elseif ( get_field('larger_header') ) { ?>
            <div class="large-inside-header">
                <div class="container">
                    <h1><?php echo roots_title(); ?></h1>
                </div>
            </div>
        <?php } else { ?> 
            <div class="inside-header">
                <div class="container">
                    <h1><?php echo roots_title(); ?></h1>
                </div>
            </div>
        <?php } ?>
    <?php endif; ?>

    <?php 
    /* Widget below Banner on all Pages */  
    dynamic_sidebar( 'preface-a' ); ?>
    


    <?php if(get_field('remove_container') ): ?>
        <div class="site-main" role="document">
            <?php include roots_template_path(); ?>
        </div>
    <?php elseif (get_field('larger_header') ): ?>
            <div class="outer-wrap-style">
                <div class="inside-wrap">
                    <div class="container">
                        <div class="large-inside">
                            <?php include roots_template_path(); ?>
                        </div>
                    </div>
                </div>
            </div>
    <?php else: ?>
      <div class="site-main wrapper" role="document">
        <div class="container">
            <div class="content row">
              <main class="main <?php echo roots_main_class(); ?>" role="main">
                <?php include roots_template_path(); ?>
              </main><!-- /.main -->
              <?php if (roots_display_sidebar()) : ?>
                <aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
                  <?php include roots_sidebar_path(); ?>
                </aside><!-- /.sidebar -->
              <?php endif; ?>
            </div><!-- /.content -->
          </div><!-- /.wrap -->
        </div>
    <?php endif; ?>

<?php if (!is_page(13) || !is_page(66) ) { ?>
        <div class="call-to-action section bg-color-tertiary bg-img-livingheritage">
            <div class="container narrow">
                <?php dynamic_sidebar( 'footer-cta' ); ?>
            </div>
        </div>
<?php } ?>


  <?php get_template_part('templates/footer'); ?>

</body>
</html>






