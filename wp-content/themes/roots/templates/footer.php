<?php $privacyPolicy = get_page_by_title( 'Privacy Policy' ); ?>

<footer class="site-footer content-info" role="contentinfo">
    <div class="footer-upper">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <?php dynamic_sidebar( 'footer-a' ); ?>
                </div>
                <div class="col-lg-2">
                    <?php dynamic_sidebar( 'footer-b' ); ?>
                </div>
                <div class="col-lg-3">
                    <?php dynamic_sidebar( 'footer-c' ); ?>
                </div>
                <div class="col-lg-3">
                    <?php dynamic_sidebar( 'footer-d' ); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <?php if(is_front_page()): ?>
                <p class="copyright"><?php bloginfo( 'name' ); ?> &copy; <?php echo date( 'Y' ); ?> - <a href="https://www.lemonadestand.org/" target="_blank">Lemonade Stand</a> | <a href="/<?php echo $privacyPolicy->post_name; ?>"><?php echo $privacyPolicy->post_title; ?></a></a></p>
            <?php else: ?>
                <p class="copyright"><?php bloginfo( 'name' ); ?> &copy; <?php echo date( 'Y' ); ?></p>
            <?php endif; ?>
        </div>
    </div>


<!-- Scripts and Analytics -->

<meta name="google-site-verification" content="z4cVZHqAckhgGfqtwqxMmGR4llUsyMUlACk5qKmGnXs" />
<meta name="msvalidate.01" content="BC07435DA6AE2F0060B716D5059E56B3" />
<script src="//a.mailmunch.co/app/v1/site.js" id="mailmunch-script" data-mailmunch-site-id="304409" async="async"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1469897449892562";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41071776-1', 'livingheritagetours.com');
  ga('send', 'pageview');

</script>

<script src="//7384.tctm.co/t.js"></script>


</footer>

<?php wp_footer(); ?>
