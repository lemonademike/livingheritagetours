<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php wp_title('|', true, 'right'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">-->

    <?php /* Google Tag Manager */ ?>
    <?php if( get_field('google_tag_manager_head', 'option') ): ?>
        <?php the_field('google_tag_manager_head', 'option'); ?>
    <?php endif; ?> 

    <?php /* Google Font */ ?>
    <?php if( get_field('google_font', 'option') ): ?>
        <?php the_field('google_font', 'option'); ?>
    <?php endif; ?>
 
    <?php /* Header Injection */ ?>
    <?php if( get_field('header_injection', 'option') ): ?>
        <?php the_field('header_injection', 'option'); ?>
    <?php endif; ?> 

    <?php wp_head(); ?>

    <?php /* Font Awesome */ ?>
    <?php if( get_field('font_awesome', 'option') ): ?>
        <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script> 
    <?php endif; ?>     

    <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo esc_url(get_feed_link()); ?>">

	<link rel="stylesheet" type="text/css" href="/wp-content/themes/roots/assets/css/style-guide.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/roots/assets/css/utility.css">

</head>
